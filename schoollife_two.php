<?php

//exemple php /var/converted/schoollife_two.php (in 61.47.40.194)

/* $folderpath = "/var/www/webroot/converter/Q/";
  $folderconvertpath = "/var/www/webroot/converter/S/";
  $pic_path = "/var/www/html/ftp/oh/image/";
  $url = "https://ftp.ving.tv/oh/image/"; */

$url = "schoollifestudiov1.ving.tv";
$imagePath = "/var/www/static/ch/";
          
while (true) {
    $ch = curl_init('https://'.$url.'/api/wowza/getpath');
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_VERBOSE, 0);
    $json = curl_exec($ch);
    curl_close($ch);
    $json = trimJson($json);
    $data = json_decode($json);

    $folderpath = "";
    $folderconvertpath = "";
    $ftp_path = "";
    $pic_path = "";
    $broadcast_path = "";


    $folderpath = $data->q_path;
    $folderconvertpath = $data->s_path;
    if (isset($data->ftp_path)) {
        $ftp_path = $data->ftp_path;
    }
    $broadcast_path = $data->broadcast_path;

    $arr_file = array();
    if ($handle = opendir($folderpath)) {
        while (false !== ($file = readdir($handle))) {
            //echo $file."\n";
            if ($file[0] == ".") {
                continue;
            }
            $arr_file[] = basename($file);
        }
        //var_dump($arr_file);
        asort($arr_file);
        foreach ($arr_file as $key => $val) {
            //echo "$key = $val\n";
            $file_src = "$folderpath" . basename($val);
            $arr_filename = explode(".", basename($val));
            $file_name = $arr_filename[0];
            $file_name_con = $file_name . ".mp4";
            $partner_id = $arr_filename[2];
            $channel_id = $arr_filename[3];
            $video_id = $arr_filename[4];
            $partner_vdo_id = $arr_filename[5];
            $flv_path = $folderconvertpath . $file_name_con;

            $ch_p = curl_init('https://'.$url.'/api/wowza/getstaticpath/tv_channel_id/' . $channel_id);
            curl_setopt($ch_p, CURLOPT_HEADER, 0);
            curl_setopt($ch_p, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch_p, CURLOPT_VERBOSE, 0);
            $json_p = curl_exec($ch_p);
            curl_close($ch_p);
            $json_p = trimJson($json_p);
            $data_p = json_decode($json_p);

            $update_pic_path = $data_p->{'static_path'}->{'path'};
            $pic_path = $data_p->{'static_path'}->{'size'};    
            
            $pic_name = $file_name . ".jpg";

            $size = explode("_", $file_name);
            //echo "=====" . $size[0];
           
            if (file_exists($file_src)) {
                $result_one = shell_exec("ffmpeg -i $file_src -threads 4 -vcodec libx264 -b 3500k -g 250 -s $size[0] -bf 3 -b_strategy 1 -coder 1 -qmin 10 -qmax 51 -sc_threshold 40 -flags +loop -cmp +chroma -me_range 16 -me_method hex -subq 5 -i_qfactor 0.71 -qcomp 0.6 -qdiff 4 -directpred 1 -flags2 +fastpskip -dts_delta_threshold 1 -acodec libfaac -ab 128000 $flv_path");

                unlink($file_src);
                $ffmpegInstance = new ffmpeg_movie($flv_path);
                echo "\nlength: " . $length = date('H:i:s', mktime(0, 0, (int) $ffmpegInstance->getDuration(), 0, 0, 0));
                echo "\ngetDuration: " . $ffmpegInstance->getDuration();
                echo "\ngetFrameCount: " . $ffmpegInstance->getFrameCount();
                echo "\ngetFrameRate: " . $ffmpegInstance->getFrameRate();
                echo "\ngetFilename: " . $ffmpegInstance->getFilename();
                echo "\ngetFrameHeight: " . $height = $ffmpegInstance->getFrameHeight();
                echo "\ngetFrameWidth: " . $width = $ffmpegInstance->getFrameWidth();
                echo "\ngetBitRate: " . $bitrate = $ffmpegInstance->getBitRate() / 1000;
                echo "\ngetVideoCodec: " . $filetype = $ffmpegInstance->getVideoCodec();

                foreach ($pic_path as $name => $value) {
                echo $name;
                echo $value;
                    
                if (file_exists($name)) {
                    $state = shell_exec("ffmpeg -i $flv_path -s $value -ss 0:00:10 -vframes 1 -f image2 -vcodec mjpeg  -y " . $name . $pic_name);
                    echo "ffmpeg -i $flv_path -s $value -ss 0:00:10 -vframes 1 -f image2 -vcodec mjpeg  -y " . $name . $pic_name;
                } /*else {
                    mkdir($imagePath . $channel_id, 0777);
                    mkdir($imagePath . $channel_id . "/tv", 0777);
                    mkdir($imagePath . $channel_id . "/tv/default", 0777);

                    if (file_exists("/var/www/static/ch/" . $channel_id . "/tv/default/")) {
                        $state = shell_exec("ffmpeg -i $flv_path -s 480x360 -ss 0:00:10 -vframes 1 -f image2 -vcodec mjpeg  -y  " . $pic_path . $pic_name);
                    }
                }*/
                }

                echo 'https://'.$url.'/api/wowza/createVideoImages?id=' . $partner_vdo_id.'&channel_id='.$channel_id . '&name=' . $pic_name . '&path=' . $update_pic_path;
                $ch_p = curl_init('https://'.$url.'/api/wowza/createVideoImages?id=' . $partner_vdo_id . '&channel_id=' . $channel_id  . '&name=' . $pic_name . '&path=' . $update_pic_path);
                curl_exec($ch_p);
                curl_close($ch_p);

                //if use mp4 must be change $filetype = "mp4" becouse $filetype="mpeg4" system does not to find file.
                $filetype = "mp4";

                $bitrate = floor($bitrate);

                echo 'https://'.$url.'/api/wowza/updatevideo/id/' . $video_id . '/status/s/length/' . $length . '/bitrate/' . $bitrate;
                $ch_s = curl_init('https://'.$url.'/api/wowza/updatevideo/id/' . $video_id . '/status/s/length/' . $length . '/bitrate/' . $bitrate);
                curl_setopt($ch_s, CURLOPT_HEADER, 0);
                curl_setopt($ch_s, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch_s, CURLOPT_VERBOSE, 0);
                $json_s = curl_exec($ch_s);
                curl_close($ch_s);
                $json_s = trimJson($json_s);
                $data_s = json_decode($json_s);

                $content_path = $data_s->path_to . ".mp4";
                
                if (file_exists($flv_path)) {
                    if (copy($flv_path, $content_path)) {
                        echo "unlink($flv_path);\n";
                        unlink($flv_path);
                    }
                }
            }
        }

        closedir($handle);
    } //end if ($handle = opendir($folderpath))
    //echo date('h:i:s') . "\n";
    echo "status : working \n";
    sleep(30);
}

function trimJson($str) {
    $pos = strpos($str, "[");
    return substr($str, $pos);
}

?>