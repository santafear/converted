<?php

//php /var/www/webroot/converter/step_two.php
//ini_set('display_errors',1);

/* $folderpath = "/var/www/webroot/converter/Q/";
  $folderconvertpath = "/var/www/webroot/converter/S/";
  $pic_path = "/var/www/html/ftp/oh/image/";
  $url = "https://ftp.ving.tv/oh/image/"; */

while (true) {
    $ch = curl_init('https://studio.ving.tv/api/wowza/getpathads/');

    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_VERBOSE, 0);
    $json = curl_exec($ch);
    curl_close($ch);
    $json = trimJson($json);
    $data = json_decode($json);


    $folderpath = $data->q_path;
    $folderconvertpath = $data->s_path;
    $broadcast_path = $data->broadcast_path;
    $pic_path = $data->image_path;

    $arr_file = array();
    if ($handle = opendir($folderpath)) {
        while (false !== ($file = readdir($handle))) {
            //echo $file."\n";
            if ($file[0] == ".") {
                continue;
            }
            $arr_file[] = basename($file);
        }
        //var_dump($arr_file);
        
        //var_dump($arr_file);
        asort($arr_file);
        foreach ($arr_file as $key => $val) {
            //echo "$key = $val\n";
            $arr_filename = explode(".", basename($val));
            $file_name = basename($val);
            $file_src = $folderpath . $file_name;
            $id = $arr_filename[2];
            $flv_path = $folderconvertpath . $arr_filename[0] . '.mp4';
            $pic_name = $arr_filename[0] . '.jpg';
            $size = explode("_", $file_name);
            //want to know size now work 1 size

            $ch_n = curl_init('https://studio.ving.tv/api/wowza/updateVideoAds/id/' . $id . '/status/Q');
            //$ch_n = curl_init('https://studio.ving.tv/api/wowza/createVideoAds/name/' . basename($file));
            
            curl_setopt($ch_n, CURLOPT_HEADER, 0);
            curl_setopt($ch_n, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch_n, CURLOPT_VERBOSE, 0);
            $json_n = curl_exec($ch_n);
            curl_close($ch_n);
            $json_n = trimJson($json_n);
            $data_n = json_decode($json_n);

            $id = $data_n->id;
            $adv_vdo_id = $data_n->adv_vdo_id;
            $file_name = $data_n->file_name;
            $path_name = $data_n->path_name;
            $length = $data_n->length;
            $file_type = $data_n->file_type;
            $width = $data_n->width;
            $height = $data_n->height;
            $bitrate = $data_n->bitrate;
            $created = $data_n->created;
            $modified = $data_n->modified;
            $status = $data_n->status;
            $convert_status = $data_n->convert_status;
            $path_form = $data_n->path_form;
            $path_to = $data_n->path_to;
            $path_image = $data_n->path_image;

            $file_src = $path_form.'.'.$id;
            $flv_path = $path_to;
            $pic_path = $path_image;

            if (file_exists($file_src)) {
                $result_one = shell_exec("ffmpeg  -i $file_src -vcodec libx264 -b 3500k -g 250 -s $size[0] -bf 3 -b_strategy 1 -coder 1 -qmin 10 -qmax 51 -sc_threshold 40 -flags +loop -cmp +chroma -me_range 16 -me_method hex -subq 5 -i_qfactor 0.71 -qcomp 0.6 -qdiff 4 -directpred 1 -flags2 +fastpskip -dts_delta_threshold 1 -acodec libfaac -ab 128000 $flv_path");

                unlink($file_src);
                $ffmpegInstance = new ffmpeg_movie($flv_path);
                echo "\nlength: " . $length = date('H:i:s', mktime(0, 0, (int) $ffmpegInstance->getDuration(), 0, 0, 0));
                echo "\ngetDuration: " . $ffmpegInstance->getDuration();
                echo "\ngetFrameCount: " . $ffmpegInstance->getFrameCount();
                echo "\ngetFrameRate: " . $ffmpegInstance->getFrameRate();
                echo "\ngetFilename: " . $ffmpegInstance->getFilename();
                echo "\ngetFrameHeight: " . $height = $ffmpegInstance->getFrameHeight();
                echo "\ngetFrameWidth: " . $width = $ffmpegInstance->getFrameWidth();
                echo "\ngetBitRate: " . $bitrate = $ffmpegInstance->getBitRate() / 1000;
                echo "\ngetVideoCodec: " . $filetype = $ffmpegInstance->getVideoCodec();

                //$pic_url = $url . $partner_id . "/" . $channel_id;

                if (file_exists($pic_path)) {
                    $state = shell_exec("ffmpeg -i $flv_path -s 480x360 -ss 0:00:10 -vframes 1 -f image2 -vcodec mjpeg  -y " . $pic_path . $pic_name);
                } else {
                    mkdir($pic_path, 0777);
                    $state = shell_exec("ffmpeg -i $flv_path -s 480x360 -ss 0:00:10 -vframes 1 -f image2 -vcodec mjpeg  -y " . $pic_path . $pic_name);
                }

                //if use mp4 must be change $filetype = "mp4" becouse $filetype="mpeg4" system does not to find file.
                $filetype = "mp4";


                /* echo "php /usr/local/converter/step_three.php $partner_id $file_name $length $filetype $width $height $bitrate $pic_url $pic_name";
                  $result_two = shell_exec("php /usr/local/converter/step_three.php $partner_id $file_name $length $filetype $width $height $bitrate $pic_url $pic_name");

                  echo "<pre>";
                  print_r($result_two);
                  echo "</pre>"; */


                $bitrate = floor($bitrate);

                $ch_s = curl_init('https://studio.ving.tv/api/wowza/updateVideoAds/id/'.$id.'/status/S/length/'.$length.'/bitrate/'.$bitrate);
                curl_setopt($ch_s, CURLOPT_HEADER, 0);
                curl_setopt($ch_s, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch_s, CURLOPT_VERBOSE, 0);
                $json_s = curl_exec($ch_s);
                curl_close($ch_s);
                $json_s = trimJson($json_s);
                $data_s = json_decode($json_s);

                $content_path = $data_s->path_to;

                if (file_exists($flv_path)) {
                    if (copy($flv_path, $content_path)) {
                        echo "unlink($flv_path);\n";
                        unlink($flv_path);
                    }
                }
            }
        }

        closedir($handle);
    } //end if ($handle = opendir($folderpath))
    echo date('h:i:s') . "\n";
    sleep(30);
}

function trimJson($str) {
    $pos = strpos($str, "[");
    return substr($str, $pos);
}

?>