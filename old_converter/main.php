<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'theme' => 'ving',
    'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
    'name'=>'Ving Dongle',

    'defaultController' => 'index',

    // preloading 'log' component
    'preload'=>array('log', 'LanguageComponent'),
    //'preload'=>array('log'),
    
    // autoloading model and component classes
    'import'=>array(
        'application.models.*',
        'application.components.*',
        'application.helpers.*',
    ),

    'modules'=>array(
        // uncomment the following to enable the Gii tool
        'gii'=>array(
            'class'=>'system.gii.GiiModule',
            'password'=>'pwk123',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters'=>array('127.0.0.1','::1'),
        ),
        'iphone',
        'adv',
	'api',
    ),

    // application components
    'components'=>array(
        'user'=>array(
            // enable cookie-based authentication
            'allowAutoLogin'=>true,
        ),
        'request'=>array(
            'enableCsrfValidation'=>true,
            'enableCookieValidation'=>true,
        ),
        // uncomment the following to enable URLs in path-format

        'urlManager'=>array(
            'class'=>'application.extensions.language.LUrlManager',
            'urlFormat'=>'path',
            'showScriptName'=>false,
            'rules'=>array(
                '<lang:[a-z]{2}>/api'=>'api',    
                '<lang:[a-z]{2}>/api/<controller:\w+>'=>'api/<controller>',
                '<lang:[a-z]{2}>/api/<controller:\w+>/<action:\w+>'=>'api/<controller>/<action>',  
                '<lang:[a-z]{2}>/gii'=>'gii',    
                '<lang:[a-z]{2}>/gii/<controller:\w+>'=>'gii/<controller>',
                '<lang:[a-z]{2}>/gii/<controller:\w+>/<action:\w+>'=>'gii/<controller>/<action>',                
                '<lang:[a-z]{2}>/'=>'/',    
                '<lang:[a-z]{2}>/<_m>/<_c>/*' => '<_m>/<_c>',
                '<lang:[a-z]{2}>/<_m>/<_c>/<_a>/*' => '<_m>/<_c>/<_a>',
                '<lang:[a-z]{2}>/<_m>/<_a>/*' => '<_m>/<_a>',
                '<lang:[a-z]{2}>/<_c>/*' => '<_c>',
                '<lang:[a-z]{2}>/<_c>/<_a>/*' => '<_c>/<_a>',
            ),
        ),
        'LanguageComponent' => array (
            'class' => 'application.extensions.language.LanguageComponent',
            'languages' => array('en','th'),
        ),
        'db'=>array(
            'connectionString' => 'mysql:host=61.47.40.194;dbname=ving_studio',
            'emulatePrepare' => true,
            'username' => 'sw_ving_studio',
            'password' => 'Dragonbreath^999',
            'charset' => 'utf8',
            'enableParamLogging' => true,
        ),
       'errorHandler'=>array(
            // use 'site/error' action to display errors
            'errorAction'=>'site/error',
        ),
        'log'=>array(
            'class'=>'CLogRouter',
            'routes'=>array(
                array(
                    'class'=>'CFileLogRoute',
                    'levels'=>'error, warning',
                ),
                // uncomment the following to show log messages on web pages
                /*
                array(
                	'class'=>'CWebLogRoute',
                ),
                */
                array(
                    'class'=>'CProfileLogRoute',
                    'report'=>'summary',  // summary or callstack
                ),
            ),
        ),  
        'uploader' => array(
            'class' => 'application.extensions.vuploader.VUploaderComponent',
            'ftp' => array(
                'host' => 'static.ving.tv',
                'port' => '21',
                'username' => 'static',
                'password' => 'veryrandom69',
                'homedir' => '/var/www/static',
            ),
        ),
        'image'=>array(
            'class'=>'application.extensions.image.CImageComponent',
            // GD or ImageMagick
            'driver'=>'GD',
        ),
    ),

    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params'=>array(
        // this is used in contact page
        'adminEmail'=>'aris@playwork.co.th',
        'backoffice_url' => 'http://webtest.vingworld.co.th/cms',
        //'static' => 'http://webtest.vingworld.co.th/cms',
        'static' => 'http://static.ving.tv:8080',
        //'backoffice_url' => 'http://cms.yoohooclub.com',
        'defaultImage' => 'http://static.ving.tv:8080/.default/nopic.png',
        'vod_server'=>'rtmpe://61.47.40.195/vod',
        'live_server'=>'rtmpe://61.47.40.195/live',
    ),
);
